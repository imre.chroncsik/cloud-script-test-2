
/*
    Implements some scripting-related helpers
    (agnostic of ARSDK, could be reused in other projects for scripting support). 
    Its most important usage is to provide functionality required by 
    other classes / modules; eg. remote-bootstrap uses an ARScript instance 
    to provide requirements for the `RemoteApp` class 
    and the `narrow-intf/arsdk-impl` module. 
    
    The implemented helpers are: 
    *   executeNative(objectId, methodName, params: any[]) => result: any
        Built on top of the executeNative_str(callString) requirement. 
    *   executeCallback(callbackJsonString) => result: string
    *   replicateScriptToNativeUnchecked(newObject: any, className, params: any[])
        Create a native-side counterpart of `newObject`, 
        an instance of the class identified by `className`, 
        using `params` as the constructor parameters. 
        Once done, the two representations (native and script) of the object 
        will be connected by a shared objectId. 
    *   replicateScriptToNativeChecked(newObject: any, className, params: any[])
        Check if `newObject` is an object that was created on the script side 
        (as opposed to being an object that was created on the native side and then 
        replicated to the script side), 
        and if so, then replicate it to the native side (see replicateScriptToNativeUnchecked()). 
    *   replicateNativeToScript(nativeObject: { className, objectId: string }) => newScriptObject: any
        Create the script-side counterpart of an object newly created on the native side. 

    Requirements of ARScript: 
    *   executeNative_str(callString) => ExecuteNativeStrResult | undefined
        Execute `callString` on the native side, 
        return string results: either a `value` or an `error` inside an ExecuteNativeStrResult. 
*/

export interface ExecuteNativeStrResult { 
    value?: string
    error?: string
}

export class ARScript {
    executeNative_str?: ((callString: string) => string) = undefined
    globalNamespace: any | undefined = undefined

    executeNative(objectId: string, methodName: string, ...params: any[]): any {
        if (this.executeNative_str === undefined) return 
        while (params.length > 0 && params[params.length - 1] === undefined)
            params.pop()
        params = params.map(p => this.scriptValueToNative(p))
       
        var strCall = JSON.stringify({
            "objectId": objectId,
            "methodName": methodName,
            "parameters": params.map(p => JSON.stringify(p))
        })
        
        var resultString: string = this.executeNative_str(strCall)
        var result: ExecuteNativeStrResult = JSON.parse(resultString)
        if (result.error !== undefined) 
            throw result.error
        if (result.value === undefined)
            throw new Error("result.value === undefined")

        const isConstructor = methodName === "new"
        const resultValue = this.nativeValueToScript(result.value, isConstructor)
        return resultValue
    }

    executeCallback(callbackJsonString: string): string {
        try { 
            const callbackObject = JSON.parse(callbackJsonString)
            const callback = this.callbacks[callbackObject.callbackId]
            if (callback === undefined)
                throw "callback not found by id: " + callbackObject.callbackId
            const params = callbackObject.params.map((p: any) => this.nativeValueToScript(p, false))
            const value = callback(...params)
            return JSON.stringify({ "value": value })
        } catch(x) { 
            return JSON.stringify({ "error": x.toString() })
        }
    }

    replicateScriptToNativeChecked(newObject: any, className: string, ...params: any[]) { 
        if (params.length > 0 && params[0] === this.arsdk__replicatingNativeToScript_) return
        this.replicateScriptToNativeUnchecked(newObject, className, ...params)
    }

    replicateScriptToNativeUnchecked(newObject: any, className: string, ...params: any[]) { 
        const nativeObject = this.executeNative("arsdk_class_" + className, "new", ...params)
        this.setObjectId(newObject, nativeObject.objectId)
    }

    replicateNativeToScript(
        nativeObject: { className: string, objectId: string }, 
        name: string | undefined = undefined)
        : any { 
        
        const class_ = this.classesByName[nativeObject.className]
        const newObject = new class_(this.arsdk__replicatingNativeToScript_)
        this.setObjectId(newObject, nativeObject.objectId)

        if (name !== undefined && this.globalNamespace !== undefined)
            this.globalNamespace[name] = newObject

        return newObject
    }


    //  private
    arsdk__replicatingNativeToScript_ = "arsdk__replicatingNativeToScript_"
    userMainFunction = null
    classesByName: { [className: string]: any } = {}
    objectsById: { [objectId: string]: any } = {}
    nextCallbackId = 0
    callbacks: { [callbackId: string]: any } = {}

    private setObjectId(object: any, objectId: string) { 
        object.objectId = objectId
        this.objectsById[objectId] = object
    }

    private scriptValueToNative(scriptValue: any) {
        if (scriptValue === null)
            return "arsdk__nil__"

        if (typeof scriptValue === "function") {
            const callbackId = this.nextCallbackId++
            this.callbacks[callbackId] = scriptValue
            return callbackId
        }
        
        return scriptValue
    }

    private nativeValueToScript(nativeValue: string, isConstructorResult: boolean): any {
        if (nativeValue === "()")
            return undefined
            
        if (typeof nativeValue === "string"
            && nativeValue.indexOf("arsdk__objectId_") !== -1) {
            const objectId = nativeValue.substring("arsdk__objectId_".length)
            return objectId
        }
            
        let parsed = JSON.parse(nativeValue)
        if (typeof parsed === "object"
            && parsed.arsdk_objectId !== undefined
            && parsed.arsdk_className !== undefined) {
            
            const nativeObject = { className: parsed.arsdk_className, objectId: parsed.arsdk_objectId }
            if (isConstructorResult) {
                return nativeObject
            } else {
                var object = this.objectsById[nativeObject.objectId]
                if (object === undefined) 
                    object = this.replicateNativeToScript(nativeObject)
                return object
            }
        }
        
        return parsed
    }
}

