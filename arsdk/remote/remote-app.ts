/*
    RemoteApp 
    *   Represents an ARScript-enabled app running on a remote device. 
    *   Handles communication with the remote app, 
        with the exception of actual network traffic 
        (that's delegated to ScriptConnectionWorker). 
    *   Implements 
        executeNative_str(callString: string): RemoteMessage | undefined
        (so it can be used to provide the same-named requirement for ARScript). 
    *   Requires: 
        *   executeCallback(callbackJsonString: string): string
            Take a string that contains a JSON-encoded object that describes a native -> script callback, 
            find and execute the callback referenced by that object, 
            return a string JSON-encoding an object with either a `value` or an `error` field.         
        *   replicateNativeToScript(nativeObject: { className: string, objectId: string}, name: string | undefined): any
            Given `nativeObject`, construct a corresponding script-side object that is a replication of 
            the native object. `nativeObject` is an object that contains `className` and `objectId` fields. 
            Return the newly constructed script object. 
            If `name` is defined, then also make it available as a global with that name. 
            (Used when exporting native-created objects to script.)
        *   These requirements are typically provided by an ARScript instance. 
    *   Note executeNative_str can block the main thread. This is intentional. 
        Script -> native calls are normally (when running on device) synchronous. 
        We want remote script execution to work pretty much exactly like on-device execution 
        (obviously with the exception of network delay), 
        so even when doing networked remote script execution, script -> native calls are still sync. 
        The way this is implemented is as follows: 
        *   RemoteApp launches a worker thread. 
            The worker is responsible for sending and receiving messages to/from the remote app. 
        *   The worker can receive messages even when the main thread is blocked in a script -> native call 
            (waiting for a result from the native side, see getCallResultForQueryMessageId()). 
        *   When receiving a message, the worker puts it into a shared buffer, 
            and notifies RemoteApp on the main thread. 
        *   The worker actually sends two notifications (of different kinds), 
            because the main thread, depending on its state, can only react to one of them 
            at any time. The two kinds of notifications are: 
            1)  A message sent using parentPort.postMessage, 
                handled in RemoteApp using worker.on('message', ...). 
                RemoteApp can only receive such messages when _not_ blocked in getCallResultForQueryMessageId(). 
            2)  Using Atomics.notify() on a shared messageLengthArray. 
                This is received by Atomics.wait() in getCallResultForQueryMessageId(). 
        *   When receiving either of the two notifications, RemoteApp reads the remote message 
            from a shared buffer and processes it. 
        *   getCallResultForQueryMessageId() doesn't return until the result of the specified call is received. 
        *   If some other message (eg. a native -> script callback) is received 
            while waiting in getCallResultForQueryMessageId(), then getCallResultForQueryMessageId() takes care 
            of dispatching it. 
            Note this may result in re-entering getCallResultForQueryMessageId() if the script-side function 
            just called from the native side calls back to native. 
        *   Once getCallResultForQueryMessageId() has a result, it returns. 
            If this was the top-level getCallResultForQueryMessageId(), then normal execution is resumed 
            (as opposed to running a remote-message loop inside getCallResultForQueryMessageId()). 
            After resuming normal execution, newly received remote messages are handled using 
            didReceiveWorkerMessage(). 
*/

import { Worker } from "worker_threads"
import * as http from "http"
import { exec } from "child_process"
import qrcode from "qrcode-generator"


export interface RemoteAppRequirements { 
    executeCallback(callbackJsonString: string): string
    replicateNativeToScript(nativeObject: { className: string, objectId: string}, name: string | undefined): any
}

interface RemoteMessage { 
    messageId?: number
    respondingToMessageId?: number
    error?: string
    value?: string
    scriptCallback?: string
    exportObjectId?: string
    exportObjectClassName?: string
    exportObjectGlobalVarName?: string
    nativeCall?: string
}

export class RemoteApp { 
    constructor(requirements: RemoteAppRequirements) {
        this.requirements = requirements

        this.remoteMessageLengthBuffer = new SharedArrayBuffer(4)
        this.remoteMessageLengthArray = new Int32Array(this.remoteMessageLengthBuffer)
        this.remoteMessageBuffer = new SharedArrayBuffer(1024)
        this.remoteMessageArray = new Uint8Array(this.remoteMessageBuffer)

        this.worker = new Worker(
            './.ts-out/arsdk/remote/script-connection-worker.js', 
            { 
                workerData: { 
                    messageLengthBuffer: this.remoteMessageLengthBuffer, 
                    messageBuffer: this.remoteMessageBuffer }})        
        this.worker.on('message', (message) => { this.didReceiveWorkerMessage(message) })

        this.launchQRServer()
    }
    
    executeNative_str(callString: string): string { 
        let messageId: number = this.sendNativeCall(callString)
        let resultString: string = this.getCallResultForQueryMessageId(messageId)
        return resultString
    }

    //  private

    private requirements: RemoteAppRequirements

    private worker: Worker
    private remoteMessageLengthBuffer: SharedArrayBuffer
    private remoteMessageLengthArray: Int32Array
    private remoteMessageBuffer: SharedArrayBuffer
    private remoteMessageArray: Uint8Array

    private nextMessageId = 1

    private launchQRServer() { 
        exec(
            "gp url 2897", 
            (_error, stdout, _stderr) => { 
                let url = stdout.trim()
                this.launchQRServerWithUrl(url)
            })
    }

    private launchQRServerWithUrl(url: string) {        
        let qr = qrcode(4, 'L')
        qr.addData(url)
        qr.make()
        let qrHtml = qr.createImgTag()
        let html = `
            <div id="qr"></div>
            <script type="text/javascript">
            document.getElementById('qr').innerHTML = '${qrHtml}'
            </script> `

        let server = http.createServer(
            (_request: http.IncomingMessage, response: http.ServerResponse) => { 
                response.end(html)
            })
        server.on('error', (error) => { console.log("qr server error: ", error) })
        server.listen(2898)
    }

    private sendNativeCall(callString: string): number { 
        return this.sendMessage({ nativeCall: callString })
    }

    private sendResponse(value: any, queryMessageId: number) { 
        value = (value === undefined) ? "nil" : value
        this.sendMessage({ value: value, respondingToMessageId: queryMessageId })
    }

    private sendError(error: string, queryMessageId: number) { 
        console.log(`sendError: queryMessageId = ${queryMessageId}, error = ${error}`)
        this.sendMessage({ error: error, respondingToMessageId: queryMessageId })
    }

    private sendMessage(messageObject: RemoteMessage) { 
        const messageId = this.nextMessageId++
        messageObject.messageId = messageId
        const messageString = JSON.stringify(messageObject) + "###arsdk-message-separator###"
        this.sendMessageString(messageString)
        return messageId
    }

    private sendMessageString(messageString: string) {
        this.worker.postMessage(messageString)
    }

    private getCallResultForQueryMessageId(queryMessageId: number): string { 
        /*
            wait for a response message from the native side, 
            but also handle cases when we receive a message other than a response 
            (eg. a native -> script call). 
        */

        while (true) { 
            let waitResult = Atomics.wait(this.remoteMessageLengthArray, 0, 0)
            if (waitResult === "timed-out") 
                throw "waitResult === timed-out"
            //  note if waitResult === "not-equal", that just means that a message was put in the buffer
            //  _before_ we called wait() here. not a problem, just proceed with reading it. 

            let messageString = this.readRemoteMessageFromSharedBuffer()
            if (messageString === undefined)
                throw Error("getCallResultForQueryMessageId: message === undefined")
            var message: RemoteMessage = JSON.parse(messageString)
            if (message.respondingToMessageId == queryMessageId) 
                return messageString
            this.handleRemoteMessage(messageString)
        }
    }

    private readRemoteMessageFromSharedBuffer(): string | undefined { 
        if (this.remoteMessageLengthArray[0] <= 0) { 
            /*
                looks like the message has already been read out from the buffer. 
                this can happen if readRemoteMessage() is called from didReceiveWorkerMessage, 
                but the worker -> main delivery of the "didReceiveRemoteMessage" notification was delayed 
                while the main thread was blocked in getCallResultForQueryMessageId(). 
                in this case by the time "didReceiveRemoteMessage" gets delivered, 
                and in response to that, readRemoteMessage() gets called, 
                getCallResultForQueryMessageId() has already read and handled the message, 
                there's nothing to do here, all is fine. 
                (if we get to this branch when called from getCallResultForQueryMessageId(), 
                that's an error; handled in getCallResultForQueryMessageId().)
            */
            return undefined
        }
        let newRemoteMessageArray = this.remoteMessageArray.slice(0, this.remoteMessageLengthArray[0])
        let newRemoteMessageString = (new TextDecoder("utf8")).decode(newRemoteMessageArray)
        this.remoteMessageLengthArray[0] = 0
        Atomics.notify(this.remoteMessageLengthArray, 0, 1)
        return newRemoteMessageString
    }

    private handleRemoteMessage(messageString: string) {
        let message: RemoteMessage = JSON.parse(messageString)
        if (message.error !== undefined)
            throw message.error
        else if (message.value !== undefined) 
            //  we should only receive value messages when waiting for a 
            //  script -> native call result in getCallResultForQueryMessageId(). 
            throw new Error("unexpected value message: " + JSON.stringify(message))
        else if (message.scriptCallback !== undefined) 
            this.handleCallbackMessage(message)
        else if (message.exportObjectId !== undefined) 
            this.handleExportObjectMessage(message)
        else 
            throw new Error("unrecognized message: " + JSON.stringify(message))
    }

    private handleCallbackMessage(message: RemoteMessage) { 
        if (this.requirements === undefined) return
        if (message.scriptCallback === undefined) return
        if (message.messageId === undefined) return
        var resultString = this.requirements.executeCallback(message.scriptCallback)
        var result = JSON.parse(resultString)
        if (result.error !== undefined) { 
            this.sendError(result.error, message.messageId)
        } else {
            this.sendResponse(result.value, message.messageId)
        }
    }

    private handleExportObjectMessage(message: RemoteMessage) { 
        if (this.requirements === undefined) return
        if (message.exportObjectClassName === undefined || message.exportObjectId === undefined) return
        if (message.messageId === undefined) return
        const nativeObject = { className: message.exportObjectClassName, objectId: message.exportObjectId }
        this.requirements.replicateNativeToScript(nativeObject, message.exportObjectGlobalVarName)
        this.sendResponse("ack", message.messageId)
    }

    private didReceiveWorkerMessage(workerMessage: string) { 
        switch (workerMessage) { 
            case "didReceiveRemoteMessage": 
                let remoteMessage = this.readRemoteMessageFromSharedBuffer()
                if (remoteMessage !== undefined)
                    this.handleRemoteMessage(remoteMessage)
                break
            default: 
                //  not a control message, just treat it as a log message
                console.log(workerMessage)
                break
        }
    }
}
