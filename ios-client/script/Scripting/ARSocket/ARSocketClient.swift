//
//  ARSocketClient.swift
//  script
//
//  Created by Imre Chroncsik on 6/5/20.
//  Copyright © 2020 Ryot. All rights reserved.
//

//  based on https://forums.swift.org/t/socket-api/19971/10

import Foundation
import Network

protocol ARSocketClient: class {
    var connection: ARSocketConnection { get }
    func start()
}

protocol ARSocketClientDelegate: class {
    func didConnect(connection: ARSocketConnection)
}

class ARSocketClientImplementation: ARSocketClient {
    weak var delegate: ARSocketClientDelegate? = nil
    let connection: ARSocketConnection

    init(server: String, port: Int) {
        let host = NWEndpoint.Host(server)
        let port = NWEndpoint.Port(rawValue: UInt16(port)) ?? .any
        let nwConnection = NWConnection(host: host, port: port, using: .tcp)
        self.connection = ARSocketConnectionImplementation(nwConnection: nwConnection)
        self.connection.delegate = self
    }
    
    func start() {
        self.connection.start()
    }
}

extension ARSocketClientImplementation: ARSocketConnectionDelegate {
    func connection(_: ARSocketConnection, didReceiveData: Data) {
    }
    
    func connectionDidStop(_ connection: ARSocketConnection, error: Error?) {
        if error == nil {
            exit(EXIT_SUCCESS)
        } else {
            exit(EXIT_FAILURE)
        }
    }
    
    func connectionIsReady(_ connection: ARSocketConnection) {
        delegate?.didConnect(connection: connection)
    }
}
